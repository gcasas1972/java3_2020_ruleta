package modelo.watsonUtil;

import com.ibm.cloud.sdk.core.security.Authenticator;
import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.watson.language_translator.v3.LanguageTranslator;
import com.ibm.watson.language_translator.v3.model.TranslateOptions;
import com.ibm.watson.language_translator.v3.model.TranslationResult;

public class Traductor {
	private String idiomaOrigen	;
	private String idiomaDestino;	
	private String textoOriginal;
	
	public Traductor(){	}
	
	
	public Traductor(String pOrigen, String pDestino, String pTextoOriginal){
		idiomaOrigen = pOrigen;
		idiomaDestino = pDestino;
		textoOriginal = pTextoOriginal;
	}


	public String getIdiomaOrigen() {
		return idiomaOrigen;
	}

	public void setIdiomaOrigen(String idiomaOrigen) {
		this.idiomaOrigen = idiomaOrigen;
	}

	public String getIdiomaDestino() {
		return idiomaDestino;
	}

	public void setIdiomaDestino(String idiomaDestino) {
		this.idiomaDestino = idiomaDestino;
	}

	public String getTextoOriginal() {
		return textoOriginal;
	}

	public void setTextoOriginal(String textoOriginal) {
		this.textoOriginal = textoOriginal;
	}
	
	
	public String getTextoTraducido(){
		   Authenticator authenticator = new IamAuthenticator("k9u-JBSZzx-ds0CdoZKS7yrRX1juig5MQsaK98663ogm");
		    LanguageTranslator service = new LanguageTranslator("2018-05-01", authenticator);	
		 TranslateOptions translateOptions = new TranslateOptions.Builder()
			        .addText(getTextoOriginal())
			        .modelId(getIdiomaOrigen() + "-" + getIdiomaDestino())
			        .build();
		 TranslationResult translationResult = service.translate(translateOptions).execute().getResult();
		return getTextoConvetido(translationResult.toString());
	}
	private String  getTextoConvetido (String ptext){
		int inicio 	= ptext.indexOf("translations");
		inicio 		+=  45;
		int fin 	= ptext.indexOf("\n", inicio +1)-1;
		String miTexto = ptext.substring(inicio, fin);
		return miTexto;
		
	}
}
