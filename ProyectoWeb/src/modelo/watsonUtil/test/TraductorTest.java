package modelo.watsonUtil.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import modelo.watsonUtil.ConstantesDeIdiomas;
import modelo.watsonUtil.Traductor;

public class TraductorTest {
	Traductor traductor;

	@Before
	public void setUp() throws Exception {
		traductor = new Traductor(ConstantesDeIdiomas.SPANISH,
								  ConstantesDeIdiomas.ENGLISH,
										"el perro es negro");
	}

	@After
	public void tearDown() throws Exception {
		traductor = null;
	}

	@Test
	public void test() {
		assertEquals("the dog is black", traductor.getTextoTraducido());
	}

}
